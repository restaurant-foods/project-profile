<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
        echo $request;
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 302;

        $user = User::where('email', '=', $request->get('email'))->first();

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else if ($user) {
            if (Hash::check($request->get('password'), $user->password)) {
                $user->generateToken();

                $status = 'success';
                $message = 'Login sukses';
                $data = $user;
                $code = 200;
            } else {
                $message = "Login gagal, password salah";
            }
        } else {
            $message = "Login gagal, user " . $request->email . " tidak ditemukan";
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function beforeLogin(Request $request)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1np:8001/api/login");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "email=value1&password=value2");

        $output = curl_exec($ch);
        curl_close($ch);

        echo $output;
        // return redirect('/login')
        //     ->with('content', $output);

        // or when your server returns json
        // $content = json_decode($response->getBody(), true);

    }

    public function loginTemplate()
    {
        return view('login');
    }
}
