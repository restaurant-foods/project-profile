/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

// window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to~
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// import BootstrapVue from 'bootstrap-vue' //Importing

// Vue.use(BootstrapVue) // Telling Vue to use this in whole application



import Vue from 'vue'
import router from './router';
import vuetify from './vuetify';
import App from './layouts/App.vue'

Vue.component('navbar', require('./components/Navbar.vue').default);

Vue.config.productionTip = false


Vue.mixin({
    methods: {
        uploadImage(index, type) {
            this.selectedImageType = type;
            this.$refs.attachment[index].$el.children[1].click();
        },

        onChangeUploadImage(e) {
            const image = e.dataUrl;
            const type = this.selectedImageType;
            if (!image) return;

            const fileBlob = this.dataURItoBlob(e.dataUrl, e.info.type);
            const file = new File([fileBlob], e.info.name, { type: fileBlob.type });

            if (type === "image") {
                this.urls.url_image = URL.createObjectURL(file);
                this.files.image = file;
            }

            document.querySelector(
                `.preview[data-identification='${type}'] .text-preview`
            ).style.display = "none";
            document.querySelector(
                `.preview[data-identification='${type}']`
            ).style.minHeight = 0;
        },
        dataURItoBlob(dataURI, contentType) {
            var byteString;
            if (dataURI.split(",")[0].indexOf("base64") >= 0)
                byteString = atob(dataURI.split(",")[1]);
            else byteString = unescape(dataURI.split(",")[1]);

            // var mimeString = dataURI
            //   .split(',')[0]
            //   .split(':')[1]
            //   .split(';')[0];

            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            var bb = new Blob([ab], { type: contentType });
            return bb;
        },

        checkImage(model) {
            for (var url in this.urls) {
                if (url === `url_${model}` && this.urls[url] !== "") return true;
            }
        },

        showImage(model) {
            for (var url in this.urls) {
                if (url === `url_${model}`) return this.urls[url];
            }
        },

        validateImage() {
            this.imageErrors = [];

            for (let image in this.files) {
                if (this.files[image]) {
                    let fileName = this.files[image].name;
                    let extension = fileName.split(".").pop();

                    const standartExtensions = ["png", "jpg", "jpeg"];
                    const isRight = standartExtensions.filter(
                        ex => ex === extension.toLowerCase()
                    );

                    if (isRight.length <= 0) {
                        this.imageErrors.push({
                            model: image,
                            error: "Format file harus berupa .png / .jpg / .jpeg"
                        });
                    }
                }
            }
        },

        checkImageError(model) {
            let isError = this.imageErrors.filter(err => err.model === model);

            if (isError.length > 0) return true;
            else return false;
        },

        getImageError(model) {
            const error = this.imageErrors.filter(err => err.model === model);
            if (error.length <= 0) return;
            return error[0].error;
        },
    },
});

const app = new Vue({
    el: '#app',
    router,
    vuetify,
    render: h => h(App)
});


